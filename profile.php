<?php 	session_start();
	$user = $_SESSION['user'];

	if( isset($user) && !empty($user)){
 ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<style type="text/css">

		body{
			margin: auto;
			background-color: #edf0f5;
		}
		#navi{
			border:1px solid;
			width: 100%;
			height: 60px;
			background-color: #4267b2;
			display: flex;
			justify-content: space-around;
			align-items: center;
		}
		#search{
			display: flex;
			justify-content: space-between;
		}
		
		#nkar{
			width: 300px;
			height: 300px;
			border: 1px solid;
			margin: 50px 0 0 50px;

		}
		#icon{
			width: 200px;
			height: 50px;
			border: 1px solid;

		}
		#frli{
			border: 1px solid;
			width: 400px;
			height: 400px;

		}
		#div1{
			width: 100%;
			height: 500px;
			display: flex;
			justify-content: space-between;
			margin: 50px 0 0 50px;
			
			/*align-items: center;*/
		}
	</style>
</head>
<body>

	<nav id="navi">

		<div id="search">

		<input class="form-control" placeholder="Search..." type="search" name="">
		<button type="button" class="btn btn-default">
      <span class="glyphicon glyphicon-search"></span> Search
   	 </button>
			
		</div>


		<div id="icon">
		<i class="fa fa-users" style="font-size:36px"></i>
		<i class="fa fa-gear" style="font-size:36px;"></i>
		<i class="material-icons" style="font-size:36px"></i>
		</div>


		<div id="logout">
		
			<a href="#" class="btn btn-default"">
          <span class="glyphicon glyphicon-off"></span> log out 
        </a>
		</div>


	</nav>


	<div id="div1">
			
	<div id="nkar"></div>

	<h1><?php echo $user['name']; ?></h1>
	<h2><?php echo $user['surname']; ?></h2>
	<h2><?php echo $user['age']; ?></h2>

	<div id="frli">
		
	</div>


	</div>




</body>
</html>


<?php }else{

	header('location:registr.php');
} ?>